\documentclass[letter,10pt]{article}
\usepackage{amsthm,amsmath,amssymb}
\usepackage{algorithm,algorithmic}
%\usepackage{natbib}
%\usepackage[numbers/authoryear]{natbib}
%\bibpunct[, ]{(}{)}{;}{a}{,}{,}
\usepackage{wrapfig,psfrag}
\usepackage{graphicx}
\graphicspath{{grf/}}
\usepackage{geometry}
\geometry{legalpaper, margin=1.2in}

\newcommand{\bgamma}{\ensuremath{\boldsymbol{\gamma}}}

\title{\textbf{Exploring the Value of Linguistic Features in Deep Architectures for Machine Comprehension}}
\author{Hai Wang and David McAllester\\
Toyota Technological Institute at Chicago}
\date{}

\begin{document}
\maketitle

This project concerns machine comprehension of natural language. More
specifically, we are interested in question answering based on a given
text about specific entities and events not mentioned in general
knowledge sources such as Wikipedia, freebase or knowledge graphs. We
are particularly interested in large scale question answering corpora
appropriate for training large neural networks. One example is the Google CNN/Dailymail
corpus. We have also prepared a corpus of over 180,000 machine comprehension problems
mechanically extracted from the LDC Gigaword corpus. We plan to explore
deep learning methods for such question answering corpora involving
deep recurrent neural network with GRU cells \cite{GRU} to encode the stories and questions
and capture the important information in the context
\cite{nips15_hermann}. Various recent papers have
demonstrated that deep recurrent neural network based question
answering system trained on large dataset can greatly improve
understanding accuracy \cite{memnetworks, end2end, lexical1, lexical2, AS, GAS, Dchen}.  \textbf{This project will combine
GRU encodings of paragraphs and sentences with more traditional semantic
features and will carry out natural language understanding
experiments on our new large corpus.}

\begin{figure}[ht]
\centering
\includegraphics[width=1.1\linewidth]{figure2.png}
\caption{{Schematic diagram of deep RNN based question answering system (left model).}}
\label{f:deep1}
\end{figure}

\begin{figure}[ht]
\centering
\includegraphics[width=1.1\linewidth]{figure1.png}
\caption{{Schematic diagram of deep RNN based question answering system (right model).}}
\label{f:deep2}
\end{figure}

Specifically, we are investigating architectures for this problem inspired by the gated-attention reader \cite{GAS} shown in figure~\ref{f:deep1} and figure~\ref{f:deep2}. Our model contains two separate deep learning models (we call them left model and right model). The left model contains stacked-RNN and feed forward neural network, and it takes the extracted linguistic features for each sentence as input. The right model contains only stacked RNN, and it takes the embedding vector for each token in the contexts. Both models output the probability for each candidate answer, but the left model calculates the probability based on the linguistic features for each sentence, while the right model outputs the probability based on the response from document context and word embedding. To combine those two outputs, we have another layer to learn the relative weights of the two models. To quantify the contribution of each sentence or each token in the context, we also apply the attention mechanism to those two models \cite{attention}.

%namely, for the left model, the final probability for each candidate answer is a summation of probability of the sentence for which the candidate answer apprears, for the right model, the final probability for the candidate answer is a combination of probability for each occurrence of candidate answer. In both models, the probability for each token is calculated by dot product between two vectors.   

Compared with the gated attention readers, our contribution mainly lies in the left model. Different from the right model, we first calculate the linguistic features for each sentence in the document, and then feed the document's linguistic features to RNN sentence by sentence, meanwhile we feed the question's linguistic features to DNN (since in general, question only contains one sentence). Similar with gated attention method, we gate the RNN response for each sentence by using the elementwise product between RNN hidden layer and the DNN output, and the final response for each sentence is calculated using the inner product between RNN hidden vector and DNN output vector. The linguistic features can contain word embedding vector for each sentence, dependency tree structure, Name Entity Recognition (NER), Part of Speech Tagging (POS), it also can contain semantical features such as sentimental analysis and their variations \cite{Dchen}, depending on the importance of feature. 

%For the right model, we feed the documents and question to RNN word by word. Before feeding to the RNN, each word is assigned the corresponding embedding vector by checking the looking up table. During the training procedure, the lookup table is also updated. Finally, the output of the two models is linearly combined to get the final similarity.          

To train the system, we use the Log loss such that the likelihood of correct answer is maximized and the likelihood of wrong answers is minimized. After training, we feed each document and question to our architecture. As a result, by checking the output of the two models, we have the likelihood for each candidate answer, by using the soft-max function, we can convert the likelihood to probability and pick the one with highest probability as the correct answer.   

The whole idea can be formulated in a well-defined objective function with network weights optimized by backpropagation. Notice that RNNs implement heavily nonlinear mappings that consist of several layers of nolinear mappings followed by elementwise activations. Therefore the major computational bottleneck of RNN and DNN is in computing the nolinear mappings at each layer, both for feeding forward to obtain the output and for backpropagation to obtain the gradient. Furthermore, the nolinear mapping is implemented as a matrix multiplication where the sizes of the matrices depends on the dataset size and hidden layer width, which lies exactly in the strength of the multi-threading techniques implemented by GPUs. Finally, since recurrent neural network is proposed for sequence learning, thus it's
more difficult to train than common neural network. Our current CPU implementation of the algorithm uses C++ and the Intel MKL library. Despite the excellent performance of recurrent neural network, it takes several hours on a workstation to carry out experiment for relatively small data(e.g., we use 60,000 documents, each of which has around 800 tokens, and each document contains one question), not to mention the time needed for tuning the architecture
for best test performance on training corpus.


\textbf{We expect to gain at least $5$--$6$ times speedup by
  implementing our algorithm with GPUs and the CUDA library, as is
  commonly observed by other researchers in the deep learning
  community \cite{optimizationondeep}.} The speedups will also allow
us to do more careful model selection and train/test the model on a
much larger question answering corpus to make the algorithm more
practical for real-world applications.

\paragraph{Requested Equipment} To allow learning and testing models for deep learning at the scale of millions of documents, I hereby apply for \textbf{two Titan X} units. Since in our architecture, we have two models, we could even utilize the two units in parallel in deep recurrent neural network to further boost the speedup, as the feeding-forward and backpropagation processes of the networks can be done independently from each other (once the error signal at the output end is computed). We plan to install both GPUs in a modern multicore workstation with 32 GB of RAM and 2 TB of hard disk at the Toyota Technological Institute at Chicago.

\paragraph{Dissemination Plan: Open-Source Sharing and Teaching} This equipment grant would provide our group with a valuable resource in accelerating our research in this data- and computation-intensive field and we would be happy to acknowledge NVIDIA's support on our professional web site and any resulting publications. We plan to share our CUDA deep learning module with the community, as well as to organize informal CUDA coding seminars for researchers and graduate students at Toyota Technological Institute at Chicago.




\begin{thebibliography}{9}

\bibitem{nips15_hermann}{2015}
Karl Moritz Hermann and Tom\'a\v{s} Ko\v{c}isk\'y and Edward Grefenstette and Lasse Espeholt.
\textit{Teaching Machines to Read and Comprehend}, NIPS, 2015.

\bibitem{memnetworks}{2015}
Jason Weston, Sumit Chopra, Antoine Bordes.
\textit{Memory Networks}, Arxiv, 2015.
 
\bibitem{end2end}{2015}
Sainbayar Sukhbaatar, Arthur Szlam, Jason Weston, Rob Fergus
\textit{End-To-End Memory Networks.}, Arxiv, 2015.

\bibitem{RCB}{2015}
Felix Hill, Antoine Bordes, Sumit Chopra \& Jason Weston.
\textit{The Goldilocks Principle: Reading Children's Books with Explicit Memory Representations}, ICLR, 2016.

\bibitem{optimizationondeep}{2015}
Quoc V. Le, Jiquan Ngiam, Adam Coates, Abhik Lahiri, Bobby Prochnow and Andrew Y. Ng
\textit{On Optimization Methods for Deep Learning}, ICML, 2011.

\bibitem{lexical1}{2015}
Wen-tau Yih Ming-Wei Chang Christopher Meek and Andrzej Pastusiak
\textit{Question Answering Using Enhanced Lexical Semantic Models}, ACL, 2013.

\bibitem{lexical2}{2015}
Hai Wang, Mohit Bansal, Kevin Gimpel and David McAllester
\textit{Machine comprehension with syntax, frames, and semantics}, ACL, 2015.

\bibitem{bp}{2015}
David E Rumelhart, Geoffrey E Hinton and Ronald J Williams
\textit{Learning representations by back-propagating errors}, Nature, 1986.

\bibitem{GRU}{2014}
Junyoung Chung, Caglar Gulcehre, KyungHyun Cho and Yoshua Bengio
Empirical Evaluation of Gated Recurrent Neural Networks on Sequence Modeling, Arxiv

\bibitem{AS}{2016}
Rudolf Kadlec, Martin Schmid, Ondrej Bajgar, Jan Kleindienst
Text Understanding with the Attention Sum Reader Network, Arxiv

\bibitem{GAS}{2016}
Bhuwan Dhingra, Hanxiao Liu, William W. Cohen, Ruslan Salakhutdinov
Gated-Attention Readers for Text Comprehension, Arxiv

\bibitem{Dchen}{2016}
Danqi Chen, Jason Bolton, Christopher D. Manning. 
A Thorough Examination of the CNN/Daily Mail Reading Comprehension Task, ACL, 2016

\bibitem{attention}{2016}
Volodymyr Mnih, Nicolas Heess, Alex Graves, Koray Kavukcuoglu
Recurrent Models of Visual Attention
\end{thebibliography}


\end{document}
